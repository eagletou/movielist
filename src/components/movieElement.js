import React, {Component} from 'react';
import './movieElement.css';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

export default class MovieElement extends Component {

    constructor(props) {
        super(props);
        this.deleteItem = this.deleteItem.bind(this);
    }

    deleteItem() {
        this.props.onDeleteItem(this.props.movieData.imdbID);
    }

    render() {
        return (
            <Col style={{margin: '20px 0'}} className="gutter-row" span={4}>
                <Card style={{ width: '18rem', height: '30rem'}} bg="dark" text="light">
                    <Card.Img className="image-size" variant="top" src={this.props.movieData.Poster} />
                    <Card.Body className="card-body">
                        <Card.Text className="font">{this.props.movieData.Year}, {this.props.movieData.Title}</Card.Text>
                        <Button className="card-button" variant="primary" onClick={this.deleteItem}>Delete</Button>
                    </Card.Body>
                </Card>
            </Col>
        )
    };
}
