import React, {Component} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Col from 'react-bootstrap/Col';
import './searchBar.css';
import axios from 'axios';
const API_KEY = '8090a6de'

export default class SearchBar extends Component {
    
    constructor(props) {
        super(props);
        this.getMovieInformation = this.getMovieInformation.bind(this);
        this.searchKeyWord = "";
    }
    
    getMovieInformation() {
        if (this.searchKeyWord !== "") {
            axios.post(`https://www.omdbapi.com/?s=${this.searchKeyWord}&apikey=${API_KEY}`)
            .then(searchResult => {
                this.props.onSearchResult(searchResult);
            });
        }
    }

    render() {
        return (
            <Navbar expand="lg" className="Search-bar">
                <Form inline>
                    <Col className="search-input">
                        <FormControl type="text" placeholder="Search movie title" onChange={t => this.searchKeyWord = t.target.value}/>
                    </Col>
                    <Col sm="1">
                        <Button variant="outline-success" onClick={this.getMovieInformation}>Search</Button>
                    </Col>
                </Form>
            </Navbar>
        );
    }
}