import React from 'react';
import './App.css';
import SearchBar from './components/searchBar';
import MovieElement from './components/movieElement';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default class App extends React.Component {

  // For future pagination
  // currentPage = 1;
  // totalResults = 0;

  state = {
    movieData: []
  };

  stateChange = (f) => {
    this.setState({
      movieData: f.data.Search
    });
  }

  // Delete movie in the array based on imdbID
  deleteItem = (imdbID) => {
    for(var i = 0; i < this.state.movieData.length; i++) {
      if(this.state.movieData[i].imdbID === imdbID) {
        this.state.movieData.splice(i, 1);
        this.setState({
          movieData: this.state.movieData
        });
      }
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
        <h3>Search Your Favourite Movie</h3>
        <SearchBar onSearchResult={this.stateChange} pageNum={this.currentPage}></SearchBar>
        <Row>
          {this.state.movieData !== null && this.state.movieData.map((result) => (
            <Col sm={2.5}><MovieElement movieData={result} onDeleteItem={this.deleteItem}/></Col>
          ))}
        </Row>
        </header>
      </div>
    );
  }
}